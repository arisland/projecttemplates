#ifndef GTKMM_MAINWINDOW_H
#define GTKMM_MAINWINDOW_H

#include <gtkmm-3.0/gtkmm.h>
#include <iostream>

class MainWindow : public Gtk::Window {
 public:
  MainWindow();
  virtual ~MainWindow();
 protected:
  Gtk::Box m_VBox;
  Gtk::Button m_Btn;
  Gtk::ScrolledWindow m_ScrolledWindow;
  Gtk::TextView m_TextView;
};

#endif //GTKMM_MAINWINDOW_H
