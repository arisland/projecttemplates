#include "mainwindow.h"

MainWindow::MainWindow() {
  set_title("Gtk::TextView example");
  set_border_width(10);
  set_default_size(800, 500);
  m_TextView.set_border_width(10);

  add(m_VBox);
  m_Btn.set_label("button here");

  m_ScrolledWindow.add(m_TextView);
  m_ScrolledWindow.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);
  m_VBox.pack_start(m_ScrolledWindow);

  show_all_children();

}

MainWindow::~MainWindow() {
  std::cout << "MainWindow destructor called\n";
}
